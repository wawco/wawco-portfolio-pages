<?php
DEFINE('WEBPHYS_PORTFOLIO_TYPE', 'webphys_portfolio');
DEFINE('WAWCO_PORTFOLIO_STORY_KEY', '_portfolio_story'); // WHat it should be
DEFINE('WAWCO_PORTFOLIO_STORY_KEY_U1', 'portfolio_story'); // What it was before build 1
DEFINE( 'WAWCO_PORTFOLIO_OPTIONS_KEY', 'wawco_portfolio_options' );
DEFINE( 'WAWCO_PORTFOLIO_SETTINGS_KEY', 'wawco_portfolio_options' );
DEFINE( 'WAWCO_PORTFOLIO_BUILD_SETTINGS_KEY', 'wawco_build_num');
DEFINE( 'WAWCO_PORTFOLIO_BUILD_NUM', 1 );  // Just increment this by one when versions are needed.  Not intended as a visible version string.


class Wawco_Portfolio_Pages {
	private static $_this;
	private static $post_title_links = array();

	// Don't want this called directly
	private function __construct() {

		//register_activation_hook( __FILE__, array( $this, 'YOUR_METHOD_NAME' ) );

		add_action( 'the_post', array($this, 'webphys_portfolio_url_grabber' ));
		add_action( 'edit_form_after_title', array($this, 'main_edit_wrapper_pre' ));
		add_action( 'edit_form_after_editor', array($this, 'main_edit_wrapper_post' ));
		add_filter( 'pre_get_posts', array($this, 'add_portfolios_to_query') );

		// Make the priorities customizable in an options screen
		add_action('init', array($this, 'add_custom_field_support'), 100);
		// TODO: Make the removal/replacement of webphys depend on an option
		add_action('init', array($this, 'replace_shortcodes'), 100);
		add_action('init', array($this, 'detect_updates'));
		add_shortcode('wawco_portfolio', array($this, 'portfolio_loop_with_links'));

		add_action('admin_init', array( $this, 'settings_init'));
		add_action('admin_init', array( $this, 'options_init'));

		if(is_admin()) {
			// TODO: Need to find a good hook to call here.  This is just in case this filter gets re-added in webphys
			remove_filter('media_upload_tabs','remove_quick_edit',10,2);

			add_action('save_post', array($this, 'save_wawco_portfolio_meta'), 5);
			add_action( 'admin_menu', array($this, 'options_menu') );
		}
	}

	// Public Methods
	function add_custom_field_support() {
		if(post_type_exists(WEBPHYS_PORTFOLIO_TYPE)) {
			add_post_type_support( WEBPHYS_PORTFOLIO_TYPE, 'custom-fields' );
		}
	}

	/**
	 * Hooks into edit_form_after_title, puts a pretty little box around the main content
	 * entry box and labels it "Portfolio Summary" to specify its purpose a little bit better.
	 *
	 * @global post_type Set somewhere in edit-form-advanced.php, specifies the post type
	 *
	 */
	function main_edit_wrapper_pre() {
		global $post_type;

		$box_label = get_option(WAWCO_PORTFOLIO_OPTIONS_KEY)['summary_label'];
		if( $post_type == WEBPHYS_PORTFOLIO_TYPE ) {
			echo '<div id="summary-postbox" class="postbox-container">';
			echo '<div id="webphys_portfolio_edit_init" class="postbox " style="display: block;">';
			echo '<h3 class="hndle"><span>'.__($box_label).'</span></h3>';
			echo '<div class="inside">';
		}
	}

	/**
	 * Hooks into edit_form_after_editor.  Just finishes the divs added in main_edit_wrapper_pre(),
	 * and then adds the "story" editor to the page (if appropriate).
	 *
	 * @global post_type Set somewhere in edit-form-advanced.php, specifies the post type
	 *
	 * @see main_edit_wrapper_pre
	 */
	function main_edit_wrapper_post() {
		global $post_type; // This comes from edit-form-advaced.php or even earlier
		global $post;
		$box_label = get_option(WAWCO_PORTFOLIO_OPTIONS_KEY)['story_label'];

		if( $post_type == WEBPHYS_PORTFOLIO_TYPE ) {
			echo '</div></div></div>';
		$post_story_content = get_post_meta($post->ID, WAWCO_PORTFOLIO_STORY_KEY, true);

		?>
		<div id="story-container" class="postbox-container">
		<div id="story-postbox" class="postbox">
		<h3 class="hndle"><span><?php _e($box_label)?></span></h3>
		<div class="inside">

		<?php wp_editor($post_story_content, 'story', array('dfw' => true, 'tabfocus_elements' => 'sample-permalink,post-preview', 'editor_height' => 360) ); ?>

		<table id="story-status-info" cellspacing="0"><tbody><tr>
			<td id="story-word-count"><?php printf( __( 'Word count: %s' ), '<span class="word-count">0</span>' ); ?></td>
		</tr></tbody></table>
		</div></div></div>
		<?php
		}
	}

	/**
	 * Hooks into 'save_post' and saves our meta data like 'story.'
	 *
	 * @param int $post_id
	 * @return boolean
	 */
	function save_wawco_portfolio_meta( $post_id ) {
		// TODO: Add nonce stuff
		if(!is_admin() && !current_user_can( 'edit_post', $post_id )) {
			return false;
		}

		// Post checks out
		if(isset($_POST['story'])) {
			$portfolio_story = wp_kses_post($_POST['story']);
			update_post_meta($post_id, WAWCO_PORTFOLIO_STORY_KEY, $portfolio_story);
		}
	}

	/**
	 * Hooks to 'init' and removes existing shortcodes to replace them with
	 * the modified ones.
	 */
	function replace_shortcodes() {
		remove_shortcode( 'webphysiology_portfolio' );
		add_shortcode('webphysiology_portfolio', array($this, 'portfolio_loop_with_links'));
	}

	/**
	 * Hook into 'the_post' in order to grab the permalinks that webphys-portfolio currently
	 * ignores.  Stores it in an associative array for later use.
	 */
	function webphys_portfolio_url_grabber() {
		global $post;
		if($post->post_type == WEBPHYS_PORTFOLIO_TYPE) {
			self::$post_title_links[trim($post->post_title)] = get_permalink();
		}
	}

	/**
	 * Adds portfolios to the query used in the loop, generally for a posts listing.
	 *
	 * @param WP_Query $query
	 */
	function add_portfolios_to_query( $query ) {
	$types = $query->get('post_type');
	if( is_home() && is_main_query() && ! is_admin() && $types != 'nav_menu_item') {
			$types = $query->get('post_type');
			$query->set('post_type', array('webphys_portfolio', 'post'));
		}
	}

	function options_menu() {
		add_options_page('Wawco Portfolio Pages Options', 'Portfolio Page Settings', 'manage_options', 'portfolio-page-settings', array($this, 'settings_output' ));
	}

	function settings_output() { ?>
<div class="wrap">
<?php screen_icon(); ?>
<h2>Your Plugin Page Title</h2>
<form method="post" action="options.php">
<?php
settings_fields(WAWCO_PORTFOLIO_OPTIONS_KEY);
do_settings_sections('wawco-portfolio-settings');
submit_button();
?>

</form>

</div>
<?php
	}

	function default_settings() {
		return array(
			'summary_label' => 'Summary',
			'story_label' => 'Story',
		);
	}

	function settings_init() {
		register_setting(WAWCO_PORTFOLIO_SETTINGS_KEY, WAWCO_PORTFOLIO_OPTIONS_KEY, array($this, 'options_validate'));
		add_settings_section('wawco-portfolio-pages-form-settings', 'Portfolio Post Form Settings', array($this, 'form_settings_section_output'), 'wawco-portfolio-settings');
		add_settings_field('wawco-portfolio-summary-title', 'Summary Label', array($this, 'summary_setting_field_output'), 'wawco-portfolio-settings', 'wawco-portfolio-pages-form-settings');
		add_settings_field('wawco-portfolio-story-title', 'Story Label', array($this, 'story_setting_field_output'), 'wawco-portfolio-settings', 'wawco-portfolio-pages-form-settings');
	}

	function options_init() {
		$options = get_option(WAWCO_PORTFOLIO_OPTIONS_KEY);
		if($options === FALSE || !isset($options['story_label']) || !isset($options['summary_label'])) {
			$options = $this->default_settings();
			update_option(WAWCO_PORTFOLIO_OPTIONS_KEY, $options);
		}
	}

	function options_validate( $input ) {
		$validated_input = array();
		$args = func_get_args();

		$validated_input['summary_label'] = sanitize_text_field($input['summary_label']);
		$validated_input['story_label'] = sanitize_text_field($input['story_label']);

		return $validated_input;
	}

	function form_settings_section_output() {
		echo "Here's a section output";
	}
	function summary_setting_field_output() {
		$options = get_option(WAWCO_PORTFOLIO_OPTIONS_KEY);
		?>
		<input type="text" name="wawco_portfolio_options[summary_label]" value="<?php echo $options['summary_label']; ?>" />
	<?php
    }

	function story_setting_field_output() {
	$options = get_option(WAWCO_PORTFOLIO_OPTIONS_KEY);
	?>
		<input type="text" name="wawco_portfolio_options[story_label]" value="<?php echo $options['story_label']; ?>"/>
	<?php
	}

	/**
	 * Wordpress shortcode.  Return HTML to display a list of portfolios.
	 * Adds links to the existing structure, if URLs corresponding to
	 * titles can be found in a lookup variable.
	 *
	 * @see webphys_portfolio_widget()
	 * @param array $atts
	 * @param string $content
	 * @return Ambigous <NULL, string>
	 */
	function portfolio_loop_with_links( $atts, $content = null ) {
		// @todo: Catch this function not being defined and report the error
		$html = portfolio_loop( $atts, $content);


		$charset = get_bloginfo( 'charset' );
		$meta_tag = '<meta http-equiv="content-type" content="text/html; charset=' . $charset . '">';
		$doc = new DomDocument();
		// Ensures the encoding of the HTML is set correctly, as loadHTML assumes an encoding that might be different.
		$html = $meta_tag.$html;
		$doc->loadHTML($html);

		$finder = new DomXPath($doc);

		$query = '//div[contains(concat(" ", normalize-space(@class), " "), " portfolio_title ")]';
		$nodes = $finder->query($query);
		$node = null;

		// Using for and not foreach because apparently major structural changes
		// can break iteration on the list.  We're making new nodes here.
		for($i = 0; $i < $nodes->length; $i++) {
			$node = $nodes->item($i);
			$text = trim($node->nodeValue);
			if(isset(self::$post_title_links[$text])) {
				$url = self::$post_title_links[$text];
				$text = '<a href="'.$url.'">'.$text.'</a>';

				//create replacement
				$replacement  = $doc->createDocumentFragment();
				$replacement  ->appendXML($text);

				$node->replaceChild( $replacement, $node->firstChild);
			}

		}
		$html = $doc->saveXML($doc->documentElement);
		return $html;
	}

	function detect_updates() {
		$saved_build = get_option(WAWCO_PORTFOLIO_BUILD_SETTINGS_KEY, NULL);
		$plugin_build = WAWCO_PORTFOLIO_BUILD_NUM;
		$updated = FALSE;

		if (is_null($saved_build) || $saved_build < $plugin_build) {
			try {
				$updated = self::run_updates($saved_build, $plugin_build);
			} catch (Exception $e) {
				$updated = FALSE;
			}
		}
		if(is_null($saved_build) || $updated) {
			update_option(WAWCO_PORTFOLIO_BUILD_SETTINGS_KEY, $plugin_build);
		}
	}

	static function instance() {
		if(!isset(self::$_this)) {
			self::$_this = new self;
		}
		return self::$_this;
	}

	// Private Methods

	/**
	 * Runs necessary updates for certain versions of the plugin.
	 *
	 * Build 1: Switch the "portfolio_story" metadata key to "_portfolio_story" so that it doesn't appear in
	 * 			custom field UI forms.
	 *
	 * @param int $saved_build
	 * @param string $plugin_build
	 */
	private function run_updates( $saved_build, $plugin_build = WAWCO_PORTFOLIO_BUILD_NUM ) {
		switch($saved_build)
		{
			case 0:
			case NULL:
				$status = get_post_stati();
				$portfolios = get_posts(array('post_type' => 'webphys_portfolio', 'numberposts' => -1, 'post_status' => 'any'));
				foreach($portfolios as $the_post) {
					$the_story = get_post_meta($the_post->ID, WAWCO_PORTFOLIO_STORY_KEY_U1, TRUE);
					if(!empty($the_story)) {
						add_post_meta($the_post->ID, WAWCO_PORTFOLIO_STORY_KEY, $the_story, TRUE);
					}
					delete_post_meta($the_post->ID, WAWCO_PORTFOLIO_STORY_KEY_U1);
					$meta = get_post_meta($the_post->ID, WAWCO_PORTFOLIO_STORY_KEY_U1);
				}
				return TRUE;
				break;
			default:
				return FALSE;
				break;
		}
	}
}
