<?php

require_once(dirname(__FILE__).'/../class-wawco-portfolio-pages.php');
require_once(dirname(__FILE__).'/../../webphysiology-portfolio/function.php');

class WawcoPortfolioPagesTemplateTest extends WP_UnitTestCase {

	public function setUp() {
		parent::setUp();

		$webphys_plugin = "webphysiology-portfolio/portfolio-main.php";
		$wawco_plugin = "wawco-portfolio-pages/wawco-portfolio-pages.php";
		$this->instance = Wawco_Portfolio_Pages::instance();

		$opt = get_option( 'show_on_front' );
		if(!is_plugin_active($webphys_plugin)) {
			activate_plugin($webphys_plugin);
		}
		if(!is_plugin_active($wawco_plugin)) {
			activate_plugin($wawco_plugin);
		}
	}

	/*
	 * @ticket 13
	*/
	public function testError404WithSingle() {
		$this->factory->post->create(array('post_type' => 'webphys_portfolio'));

		$this->go_to(home_url());

		$is404 = is_404();

		$this->assertFalse($is404, "Getting all posts with single Portoflio triggers 404 error.  A patch needs to be applied to webphysiology portfolio.");
	}

	public function go_to($url) {
		parent::go_to($url);
		// WP Test's go_to bypasses index.php and its includes and requires, which means the following pieces don't happen
		$template = get_index_template();
		$template = apply_filters( 'template_include', $template );
	}
}
