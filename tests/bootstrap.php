<?php
/** Load WordPress test environment
 * https://unit-tests.svn.wordpress.org/trunk
 *
 * If not using PHPUnit directly (such as relying on MakeGood),
 * an adjustment needs to be made at wordpress-tests'
 * boostrap.
 *
 * @see http://core.trac.wordpress.org/ticket/26725
 */
// The path to wordpress-tests
$path = 'D:/development/wordpress-tests/includes/bootstrap.php';
$_SERVER['KERNEL_DIR'] = '';
print_r($_SERVER);

if( file_exists( $path ) ) {
	require_once $path;
} else {
	exit( "Couldn't find path to wordpress-tests bootstrap.php\n" );
}
