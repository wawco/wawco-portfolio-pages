<?php

require_once(dirname(__FILE__).'/../class-wawco-portfolio-pages.php');
require_once(dirname(__FILE__).'/../../webphysiology-portfolio/function.php');

class WawcoPortfolioPagesShortcodeTest extends WP_UnitTestCase {

	public function setUp() {
		parent::setUp();
		portfolio_install();
		$this->plugin = Wawco_Portfolio_Pages::instance();

		add_shortcode('portfolio-loop', array($this->plugin, 'portfolio_loop_with_links'));
	}

	/*
	 * @ticket 16
	 *
	 * Test assumes PHP is using UTF8 encoding.
	 */
	public function testShortcodeSpaceeEncodingIssue() {
		$content = <<<HTML
born!\xc2\xa0Tap the icons, make noise... and get a surprise after you tap all 5!
HTML;
		$post = $this->factory->post->create_and_get(array('post_type' => 'webphys_portfolio', 'post_content' => $content));
		add_post_meta($post->ID, '_sortorder', 1);
		$wp_query = new WP_Query();
		$wp_query->query(array( 'post_type' => 'webphys_portfolio'));
	    $yes = $wp_query->have_posts();

		$shortcode = do_shortcode('[portfolio-loop]');

		$this->assertNotContains("\xc3\x82", $shortcode, "Shortcode encoding problem: space is double-encoded into c3 82 UTF8 character");
	}
}