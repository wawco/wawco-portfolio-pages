<?php
require_once(dirname(__FILE__).'/../class-wawco-portfolio-pages.php');

class WawcoPortfolioPagesTest extends WP_UnitTestCase {
	public $plugin_slug = 'wawco-portfolio-pages';

	public function setUp() {
		parent::setUp();
		$this->my_plugin = Wawco_Portfolio_Pages::instance();
		define('WAWCO_PORTFOLIO_BUILD_NUM', 1);
	}

	public function test_detect_updates_no_version_installed() {
		$this->assertNull(get_option(WAWCO_PORTFOLIO_BUILD_SETTINGS_KEY, NULL), "Version number present.");
		$this->my_plugin->detect_updates();
		$this->assertEquals(WAWCO_PORTFOLIO_BUILD_NUM, get_option(WAWCO_PORTFOLIO_BUILD_SETTINGS_KEY, NULL), "Wrong build setting value");
	}

	public function test_detect_updates_same_version_installed() {
		update_option(WAWCO_PORTFOLIO_BUILD_SETTINGS_KEY, 1);
		$this->my_plugin->detect_updates();
		// Need some better assertions... this test will pass even if detect_updates() does nothing.
		$this->assertEquals(WAWCO_PORTFOLIO_BUILD_NUM, get_option(WAWCO_PORTFOLIO_BUILD_SETTINGS_KEY, NULL), "Wrong build setting value");

	}

	public function test_detect_updates_no_version_found() {
		delete_option(WAWCO_PORTFOLIO_BUILD_SETTINGS_KEY);
		$this->my_plugin->detect_updates();
		$this->assertEquals(WAWCO_PORTFOLIO_BUILD_NUM, get_option(WAWCO_PORTFOLIO_BUILD_SETTINGS_KEY, NULL), "Wrong build setting value");
	}

	public function test_detect_updates_older_version_installed() {
		update_option(WAWCO_PORTFOLIO_BUILD_SETTINGS_KEY, 0);
		$this->my_plugin->detect_updates();
		$this->assertEquals(WAWCO_PORTFOLIO_BUILD_NUM, get_option(WAWCO_PORTFOLIO_BUILD_SETTINGS_KEY, NULL), "Wrong build setting value");
	}

	public function test_detect_updates_newer_version_installed() {
		update_option(WAWCO_PORTFOLIO_BUILD_SETTINGS_KEY, 2);
		$this->my_plugin->detect_updates();
		$option = get_option(WAWCO_PORTFOLIO_BUILD_SETTINGS_KEY, NULL);
		$this->assertLessThan(get_option(WAWCO_PORTFOLIO_BUILD_SETTINGS_KEY, NULL), WAWCO_PORTFOLIO_BUILD_NUM, "Wrong build setting value");
	}

	/**
	 * Tests to make sure the custom fields being used to hold "the story" meta are moved correctly to a hidden meta
	 */
	public function test_update_meta_data_migration_update_0_to_1() {
		$content = $this->generateContent();
		$posts = $content['posts'];
		$generated_meta = $content['generated_meta'];
		update_option(WAWCO_PORTFOLIO_BUILD_SETTINGS_KEY, 0);

		$this->my_plugin->detect_updates();

		$full_posts = get_posts(array('post_type' => 'webphys_portfolio'));

		$this->assertNotEmpty($posts, 'No posts to verify.');
		foreach($posts as $post_id) {
			$this->assertEmpty(get_post_meta($post_id, WAWCO_PORTFOLIO_STORY_KEY_U1), "Old metadata still detected after migration to build 1 data key.");
			$new_meta = get_post_meta($post_id, WAWCO_PORTFOLIO_STORY_KEY, TRUE);
			$this->assertNotEmpty(get_post_meta($post_id, WAWCO_PORTFOLIO_STORY_KEY), "Correct metadata keys not present or incorrectly empty. ");
			$this->assertEquals($new_meta, $generated_meta[$post_id], "Values were not correctly copied during update.");
		}
	}

	public function test_update_meta_data_migration_update_null_to_1() {
		$content = $this->generateContent();
		$posts = $content['posts'];
		$generated_meta =$content['generated_meta'];
		delete_option(WAWCO_PORTFOLIO_BUILD_SETTINGS_KEY);

		$this->my_plugin->detect_updates();

		$this->assertNotEmpty($posts, 'No posts to verify.');

		foreach($posts as $post_id) {
			$this->assertEmpty(get_post_meta($post_id, WAWCO_PORTFOLIO_STORY_KEY_U1), "Old metadata still detected after migration to build 1 data key.");
			$new_meta = get_post_meta($post_id, WAWCO_PORTFOLIO_STORY_KEY, TRUE);
			$this->assertNotEmpty(get_post_meta($post_id, WAWCO_PORTFOLIO_STORY_KEY), "Correct metadata keys not present or incorrectly empty. ");
			$this->assertEquals($new_meta, $generated_meta[$post_id], "Values were not correctly copied during update.");
		}
	}

	/**
	 * Update function was only migrating the data in published posts, and not fixing drafts or others.
	 */
	public function test_update_meta_data_published_or_otherwise() {
		$content = $this->generateContent(array('post_status' => 'draft'));
		$content =  array_merge_recursive($content, $this->generateContent());
		$posts = $content['posts'];
		$generated_meta = $content['generated_meta'];
		update_option(WAWCO_PORTFOLIO_BUILD_SETTINGS_KEY, 0);

		$this->my_plugin->detect_updates();

		$this->assertNotEmpty($posts, 'No posts to verify.');

		foreach($posts as $post_id) {
			$this->assertEmpty(get_post_meta($post_id, WAWCO_PORTFOLIO_STORY_KEY_U1), "Old metadata still detected after migration.");
			$new_meta = get_post_meta($post_id, WAWCO_PORTFOLIO_STORY_KEY, TRUE);
			$this->assertNotEmpty(get_post_meta($post_id, WAWCO_PORTFOLIO_STORY_KEY), "Correct metadata keys not present or incorrectly empty. ");
			$this->assertEquals($new_meta, $generated_meta[$post_id], "Values were not correctly copied during update.");
		}
	}

	private function generateContent($args = array()) {
		$args += array( 'post_type' => 'webphys_portfolio' );
		$content['posts'] = $this->factory->post->create_many(10, $args);
		$content['generated_meta'] = array();
		foreach($content['posts'] as $post_id) {
			$content['generated_meta'][$post_id] = rand(0, 500);
			add_post_meta($post_id, WAWCO_PORTFOLIO_STORY_KEY_U1, $content['generated_meta'][$post_id], TRUE);
		}

		return $content;
	}

	/**
	 * Detecting issue #16: the portfolio summary encoding was getting messed up by loadHTML/saveHTML.
	 */
}
